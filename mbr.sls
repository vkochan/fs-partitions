;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2019 Gwen Weinholt
;; SPDX-License-Identifier: MIT
#!r6rs

;;; MBR partition tables

(library (fs partitions mbr)
  (export
    read-mbr
    create-mbr!

    make-mbr mbr?
    sexp->mbr

    make-mbrpart mbrpart?
    mbrpart-raw-start-lba
    mbrpart-bootable?
    mbrpart-start-chs
    mbrpart-end-chs

    make-chs chs?
    chs=?
    lba->chs
    chs-cylinder chs-head chs-sector)
  (import
    (rnrs (6))
    (fs partitions common))

(define-record-type mbr
  (sealed #t) (parent parttable))

(define-record-type mbrpart
  (sealed #t)
  (parent part)
  (fields raw-start-lba
          bootable?
          start-chs end-chs))

(define-record-type chs
  (sealed #t)
  (fields cylinder head sector))

(define (chs=? chs1 chs2)
  (and (eqv? (chs-cylinder chs1) (chs-cylinder chs2))
       (eqv? (chs-head chs1) (chs-head chs2))
       (eqv? (chs-sector chs1) (chs-sector chs2))))

(define (lba->chs lba)
  (define heads/cylinder 255)
  (define sectors/track   63)
  (let* ((cylinder (fxdiv lba (fx* heads/cylinder sectors/track)))
         (temp (fxmod lba (fx* heads/cylinder sectors/track)))
         (head (fxdiv temp sectors/track))
         (sector (fx+ (fxmod temp sectors/track) 1)))
    (make-chs cylinder head sector)))

;; System IDs or partition types
(define SYSTEM-ID-EBR-CHS #x05)
(define SYSTEM-ID-EBR-LBA #x0F)

;; XXX: There are some more fields
(define MBR-OFFSET-DISK-SIGNATURE #x1B8)
(define MBR-OFFSET-1 #x1BE)
(define MBR-OFFSET-2 #x1CE)
(define MBR-OFFSET-3 #x1DE)
(define MBR-OFFSET-4 #x1EE)
(define MBR-OFFSET-SIGNATURE #x1FE)

(define (parse-mbrpart blk number offset lba-offset)
  (define (read-chs offset)
    (let ((head (bytevector-u8-ref blk offset))
          (ch/sector (bytevector-u8-ref blk (fx+ offset 1)))
          (cl (bytevector-u8-ref blk (fx+ offset 2))))
      (let ((cylinder (fxior cl (fxarithmetic-shift-right ch/sector 7)))
            (sector (fxbit-field ch/sector 0 6)))
        (and (not (eqv? 0 sector))
             (make-chs cylinder head sector)))))
  (let ((flags (bytevector-u8-ref blk offset))
        (start-chs (read-chs (fx+ offset 1)))
        (system-id (bytevector-u8-ref blk (fx+ offset 4)))
        (end-chs (read-chs (fx+ offset 5)))
        (start-lba (bytevector-u32-ref blk (fx+ offset 8) (endianness little)))
        (total-lba (bytevector-u32-ref blk (fx+ offset 12) (endianness little))))
    ;; FIXME: This check is pretty weak
    (and (or (eqv? flags 0) (eqv? flags #x80))
         start-chs end-chs
         (not (eqv? 0 total-lba))
         (not (eqv? 0 system-id))
         (make-mbrpart number
                       (+ lba-offset start-lba)
                       (+ lba-offset start-lba total-lba -1)
                       total-lba
                       system-id
                       start-lba
                       (fxbit-set? flags 7)
                       start-chs end-chs))))

;; Parse a linked list of extended partitions.
(define (parse-ebr read-sector ebr-part)
  (let ((ebr-offset (part-start-lba ebr-part)))
    (let f ((partnum 5) (part ebr-part))
      (cond
        ((not part)
         '())
        ((fx>=? partnum 100)              ;arbitrary limit
         (list part))
        (else
         (let* ((part-offset (part-start-lba part))
                (blk (read-sector part-offset)))
           (if (and (bytevector? blk) (fx>=? (bytevector-length blk) 512)
                    (eqv? #xAA55 (bytevector-u16-ref blk MBR-OFFSET-SIGNATURE
                                                     (endianness little))))
               (let ((logical-part (parse-mbrpart blk partnum MBR-OFFSET-1 part-offset))
                     (next-ebr (parse-mbrpart blk (fx- (fx+ partnum 1)) MBR-OFFSET-2 ebr-offset)))
                 (cons part (cons logical-part (f (fx+ partnum 1) next-ebr))))
               (list part))))))))

;; Parse a disk's MBR and extended partitions. The read-sector
;; procedure takes an LBA returns the sector data from the disk as a
;; bytevector (or returns the eof object in case of an error).
(define (read-mbr read-sector)
  (let ((blk0 (read-sector 0)))
    (or
      (and (bytevector? blk0) (fx>=? (bytevector-length blk0) 512)
           (eqv? #xAA55 (bytevector-u16-ref blk0 MBR-OFFSET-SIGNATURE (endianness little)))
           (let ((parts
                  (filter values
                          (list (parse-mbrpart blk0 1 MBR-OFFSET-1 0)
                                (parse-mbrpart blk0 2 MBR-OFFSET-2 0)
                                (parse-mbrpart blk0 3 MBR-OFFSET-3 0)
                                (parse-mbrpart blk0 4 MBR-OFFSET-4 0))))
                 (signature (bytevector-u32-ref blk0 MBR-OFFSET-DISK-SIGNATURE (endianness little))))
             (let ((ext-part (find (lambda (part)
                                     ;; There is only one EBR link in the MBR
                                     (and (mbrpart? part)
                                          (let ((id (part-type part)))
                                            (or (eqv? id SYSTEM-ID-EBR-CHS)
                                                (eqv? id SYSTEM-ID-EBR-LBA)))))
                                   parts)))
               (make-mbr #t signature
                         (apply append
                                (map (lambda (part)
                                       (if (eq? part ext-part)
                                           (parse-ebr read-sector part)
                                           (list part)))
                                     parts))))))
      (make-parttable #f 0 '()))))

(define (symbol->part-type symb)
  (case symb
    ((fat12) #x01)
    ((fat16<32m) #x04)
    ((extended) #x05)
    ((fat16) #x06)
    ((fat32-chs) #x0B)
    ((fat32) #x0C)
    ((linux-swap) #x82)
    ((linux) #x83)
    ((linux-lvm) #x8E)
    ((freebsd) #xA5)
    ((openbsd) #xA6)
    ((netbsd) #xA9)
    ((gpt) #xEE)
    ((efi) #xEF)
    (else (error 'symbol->part-type "Unknown partition type name" symb))))

(define (sexp->mbr lst)
  (define (sexp->part num lst)
    (let loop ((type 0) (boot? #f) (start 1) (end #f) (size #f) (lst lst))
      (if (null? lst)
          (make-mbrpart num
            start
            (or end (+ start size -1))
            (or size (+ (- end start) 1))
            type
            start
            boot?
            (lba->chs start)
            (lba->chs (or end (+ start size -1))))
          (let ((kv (car lst)))
            (if (and (list? kv) (not (null? kv)))
                (let ((k (car kv)))
                  (if (>= (length kv) 2)
                      (case k
                        ((type)
                         (let ((type (cond ((symbol? (cadr kv)) (symbol->part-type (cadr kv)))
                                           ((number? (cadr kv)) (cadr kv))
                                           (else (error 'sexp->mbr "Invalid [type] value" type)))))
                           (loop type boot? start end size (cdr lst))))
                        ((boot?)
                         (let ((boot? (cond ((boolean? (cadr kv)) (cadr kv))
                                            (else (error 'sexp->mbr "Invalid [boot?] value" boot?)))))
                           (loop type boot? start end size (cdr lst))))
                        ((start)
                         (let ((start (cond ((number? (cadr kv)) (cadr kv))
                                            (else (error 'sexp->mbr "Invalid [start] value" start)))))
                           (loop type boot? start end size (cdr lst))))
                        ((end)
                         (let ((end (cond ((number? (cadr kv)) (cadr kv))
                                          (else (error 'sexp->mbr "Invalid [end] value" end)))))
                           (loop type boot? start end size (cdr lst))))
                        ((size)
                         (let ((size (cond ((number? (cadr kv)) (cadr kv))
                                           (else (error 'sexp->mbr "Invalid [size] value" size)))))
                           (loop type boot? start end size (cdr lst))))
                        (else
                         (loop type boot? start end size (cdr lst))))
                      (loop type boot? start end size (cdr lst))))
                (loop type boot? start end size (cdr lst)))))))

  (let loop ((disk-id 0) (parts '()) (lst lst))
    (if (null? lst)
        (make-mbr #t disk-id parts)
        (let ((kv (car lst)))
          (if (and (list? kv) (not (null? kv)))
              (let ((k (car kv)))
                (if (>= (length kv) 2)
                    (case k
                      ((disk-id)
                       (let ((disk-id (cadr kv)))
                         (when (not (number? disk-id))
                           (error 'sexp->mbr "Invalid disk-id value" disk-id))
                         (loop disk-id parts (cdr lst))))
                      ((part)
                       (loop disk-id
                             (append parts
                                     (list (sexp->part (+ (length parts) 1) (cdr kv))))
                             (cdr lst)))
                      (else
                       (loop disk-id parts (cdr lst))))
                    (loop disk-id parts (cdr lst))))
              (loop disk-id parts (cdr lst)))))))

;; TODO support extended partition
(define (create-mbr! mbr read-blk write-blk!)
  (define (write-chs! bv offset chs)
    (bytevector-u8-set! bv (+ offset 0) (min (chs-head chs) #xFF))
    (bytevector-u8-set! bv (+ offset 1) (min (chs-sector chs) #xFF))
    (bytevector-u8-set! bv (+ offset 2) (min (chs-cylinder chs) #xFF)))

  (let ((blk0 (read-blk 0)))
    (bytevector-u32-set! blk0  MBR-OFFSET-DISK-SIGNATURE (parttable-disk-id mbr) (endianness little))
    (for-each
      (lambda (p)
        (when (<= (part-number p) 4)
          (let ((offset (fx+ MBR-OFFSET-1 (fx* 16 (fx- (part-number p) 1)))))
            (bytevector-u8-set! blk0 offset (if (mbrpart-bootable? p) #x80 #x00))
            (write-chs! blk0 (+ offset #x01) (mbrpart-start-chs p))
            (bytevector-u8-set! blk0 (+ offset #x04) (part-type p))
            (write-chs! blk0 (+ offset #x05) (mbrpart-end-chs p))
            (bytevector-u32-set! blk0 (+ offset #x08) (min (part-start-lba p) #xFFFFFFFF) (endianness little))
            (bytevector-u32-set! blk0 (+ offset #x0C) (min (part-total-lba p) #xFFFFFFFF) (endianness little)))))
      (parttable-partitions mbr))
    (bytevector-u16-set! blk0 MBR-OFFSET-SIGNATURE #xAA55 (endianness little))
    (write-blk! blk0 0))))
