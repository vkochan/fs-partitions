;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2019 Gwen Weinholt
;; SPDX-License-Identifier: MIT
#!r6rs

;;; GPT partition tables

(library (fs partitions gpt)
  (export
    read-gpt
    create-gpt!
    create-gpt+mbr!

    make-gpt gpt?
    gpt-revision
    gpt-alternate-lba
    gpt-first-usable-lba
    gpt-last-usable-lba

    make-gptpart gptpart?
    gptpart-guid
    gptpart-attributes
    gptpart-name

    sexp->gpt)
  (import
    (rnrs (6))
    (uuid)
    (hashing crc)
    (struct pack)
    (fs partitions common)
    (fs partitions mbr))

(define-crc crc-32)

(define-record-type gpt
  (sealed #t)
  (parent parttable)
  (fields revision
          alternate-lba
          first-usable-lba
          last-usable-lba))

(define-record-type gptpart
  (sealed #t)
  (parent part)
  (fields guid attributes name))

(define GPT-SIGNATURE #x5452415020494645) ;"EFI PART"
(define zero-guid (make-bytevector 16 0))

;; Convert to RFC 4122 format. Please explain how this happened...
(define (guid-fixup bv)
  (let-values ([(time_low time_mid time_hi_and_version clk_seq_hi_res clock_seq_low
                          node:16 node:32)
                (unpack "<LSS CC !SL" bv)])
    (pack "!LSS CC SL" time_low time_mid time_hi_and_version clk_seq_hi_res
          clock_seq_low node:16 node:32)))

(define (guid-swap bv)
  (let-values ([(time_low time_mid time_hi_and_version clk_seq_hi_res clock_seq_low
                          node:16 node:32)
                (unpack "!LSS CC SL" bv)])
    (pack "<LSS CC !SL" time_low time_mid time_hi_and_version clk_seq_hi_res
          clock_seq_low node:16 node:32)))

(define (string-truncate-nul str)
  (call-with-string-output-port
    (lambda (p)
      (let lp ((i 0))
        (unless (or (eqv? i (string-length str))
                    (eqv? #\nul (string-ref str i)))
          (put-char p (string-ref str i))
          (lp (fx+ i 1)))))))

;; Read a GPT partition entry array.
(define (read-gptpart-array read-sector partition-entry-lba entries entry-size)
  (if (> (* entries entry-size) (* 1024 1024)) ;XXX: arbitrary limit
      #f
      (let ((array (make-bytevector (* entries entry-size))))
        (let lp ((lba partition-entry-lba) (offset 0))
          (if (= offset (bytevector-length array))
              array
              (let ((blk (read-sector lba)))
                (if (eof-object? blk)
                    #f                  ;read error
                    (let ((len (fxmin (bytevector-length blk)
                                      (fx- (bytevector-length array) offset))))
                      (bytevector-copy! blk 0 array offset len)
                      (lp (+ lba 1) (+ offset len))))))))))

(define (parse-gptpart-array array entries entry-size)
  (call-with-port (open-bytevector-input-port array)
    (lambda (p)
      (let lp ((n 1))
        (if (fx=? (fx+ entries 1) n)
            '()
            (let* ((type-guid (guid-fixup (get-bytevector-n p 16)))
                   (guid (guid-fixup (get-bytevector-n p 16))))
              (let-values ([(start-lba end-lba attributes) (get-unpack p "<QQQ")])
                (let ((name (string-truncate-nul
                             (utf16->string (get-bytevector-n p 72) (endianness little)))))
                  (get-bytevector-n p (fx- entry-size 128))
                  (if (and (equal? type-guid zero-guid)
                           (equal? guid zero-guid)
                           (eqv? start-lba 0)
                           (eqv? end-lba 0)
                           (eqv? attributes 0))
                      (lp (fx+ n 1))
                      (cons (make-gptpart n start-lba end-lba (+ (- end-lba start-lba) 1)
                                          type-guid guid attributes name)
                            (lp (fx+ n 1))))))))))))

;; Compute the GPT header CRC. Returns #f if there is an error.
(define (gpt-header-crc block)
  (let ((header-size (unpack "<8x4xL" block)))
    (and (< header-size (bytevector-length block))
         (let ((header (make-bytevector header-size)))
           (bytevector-copy! block 0 header 0 (bytevector-length header))
           (bytevector-u32-native-set! header 16 0)
           (crc-32 header)))))

;; Parse a GPT header. The disk has two of these.
(define (parse-gpt-header read-sector lba)
  (let ((blk (read-sector lba)))
    (or
      (and (bytevector? blk)
           (fx>=? (bytevector-length blk) (format-size "<QLLL4x 4Q 16x QLLL"))
           (let-values ([(signature rev header-size crc my-lba alternate-lba
                                    first-usable-lba last-usable-lba
                                    array-lba entries entry-size array-crc)
                         (unpack "<QLLL4x 4Q 16x QLLL" blk)])
             (and (= GPT-SIGNATURE signature)
                  (eqv? my-lba lba)
                  (eqv? crc (gpt-header-crc blk))
                  (>= entry-size 128)
                  (let ((swapped-guid (make-bytevector 16)))
                    (bytevector-copy! blk (format-size "<QLLL4x 4Q") swapped-guid 0 16)
                    (let ((disk-guid (guid-fixup swapped-guid))
                          (raw-array (read-gptpart-array read-sector array-lba
                                                         entries entry-size)))
                      (and raw-array
                           (eqv? array-crc (crc-32 raw-array))
                           (let ((array (parse-gptpart-array raw-array entries entry-size)))
                             (make-gpt #t disk-guid (filter gptpart? array)
                                       rev alternate-lba first-usable-lba
                                       last-usable-lba))))))))
      (make-parttable #f #f '()))))

;; Parse a disk's GPTs. Returns the primary GPT and the backup GPT. If
;; the primary GPT is not valid then the backup GPT is read from the
;; last LBA of the medium.
(define (read-gpt read-sector last-lba)
  (let* ((primary (parse-gpt-header read-sector 1))
         (backup (cond ((parttable-valid? primary)
                        (parse-gpt-header read-sector (gpt-alternate-lba primary)))
                       (last-lba
                        (parse-gpt-header read-sector last-lba))
                       (else
                        (make-parttable #f #f '())))))
    (values primary backup)))

(define (symbol->part-type sym)
  (string->uuid
    (case sym
      ((bios-boot) "21686148-6449-6E6F-744E-656564454649")
      ((efi) "C12A7328-F81F-11D2-BA4B-00A0C93EC93B")
      ((freebsd) "516E7CB4-6ECF-11D6-8FF8-00022D09712B")
      ((freebsd-boot) "83BD6B9D-7F41-11DC-BE0B-001560B84F0F")
      ((freebsd-swap) "516E7CB5-6ECF-11D6-8FF8-00022D09712B")
      ((freebsd-ufs) "516E7CB6-6ECF-11D6-8FF8-00022D09712B")
      ((freebsd-zfs) "516E7CBA-6ECF-11D6-8FF8-00022D09712B")
      ((netbsd-ccd) "2DB519C4-B10F-11DC-B99B-0019D1879648")
      ((netbsd-cgd) "2DB519EC-B10F-11DC-B99B-0019D1879648")
      ((netbsd-ffs) "49F48D5A-B10E-11DC-B99B-0019D1879648")
      ((netbsd-lfs) "49F48D82-B10E-11DC-B99B-0019D1879648")
      ((netbsd-swap) "49F48D32-B10E-11DC-B99B-0019D1879648")
      ((linux) "0FC63DAF-8483-4772-8E79-3D69D8477DE4")
      ((linux-swap) "0657FD6D-A4AB-43C4-84E5-0933C84B4F4F")
      ((linux-home) "933AC7E1-2EB4-4F13-B844-0E14E2AEF915")
      ((linux-srv) "3B8F8425-20E0-4F3B-907F-1A25A76F98E8")
      ((linux-var) "4D21B016-B534-45C2-A9FB-5C16E091FD2D")
      ((linux-lvm) "E6D6D379-F507-44C2-A23C-238F2A3DF928")
      ((ms-basic-data) "EBD0A0A2-B9E5-4433-87C0-68B6B72699C7")
      ((openbsd) "824CC7A0-36A8-11E3-890A-952519AD3F61")
      (else (error 'symbol->part-type "Invalid partition type name" sym)))))

(define (sexp->gpt lst)
  (define (sexp->part num lst)
    (let loop ((type 0) (id #f) (name #f) (start 34) (end #f) (size #f) (lst lst))
      (if (null? lst)
          (make-gptpart num
            start
            (or end (+ start size -1))
            (or size (+ (- end start) 1))
            (or type zero-guid)
            (or id (random-uuid))
            0 name)
          (let ((kv (car lst)))
            (if (and (list? kv) (not (null? kv)))
                (let ((k (car kv)))
                  (if (>= (length kv) 2)
                      (case k
                        ((type)
                         (let ((type (cond ((symbol? (cadr kv)) (symbol->part-type (cadr kv)))
                                           ((string? (cadr kv)) (string->uuid (cadr kv)))
                                           (else (error 'sexp->part "Invalid [type] value" type)))))
                           (loop type id name start end size (cdr lst))))
                        ((id)
                         (let ((id (cond ((string? (cadr kv)) (string->uuid (cadr kv)))
                                            (else (error 'sexp->part "Invalid [id] value" id)))))
                           (loop type id name start end size (cdr lst))))
                        ((name)
                         (let ((name (cond ((string? (cadr kv)) (cadr kv))
                                            (else (error 'sexp->part "Invalid [name] value" name)))))
                           (loop type id name start end size (cdr lst))))
                        ((start)
                         (let ((start (cond ((number? (cadr kv)) (cadr kv))
                                            (else (error 'sexp->part "Invalid [start] value" start)))))
                           (loop type id name start end size (cdr lst))))
                        ((end)
                         (let ((end (cond ((number? (cadr kv)) (cadr kv))
                                          (else (error 'sexp->part "Invalid [end] value" end)))))
                           (loop type id name start end size (cdr lst))))
                        ((size)
                         (let ((size (cond ((number? (cadr kv)) (cadr kv))
                                           (else (error 'sexp->part "Invalid [size] value" size)))))
                           (loop type id name start end size (cdr lst))))
                        (else
                         (loop type id name start end size (cdr lst))))
                      (loop type id name start end size (cdr lst))))
                (loop type id name start end size (cdr lst)))))))

  (define (find-part-start/end parts)
    (let loop ((start 0) (end 0) (parts parts))
      (if (null? parts)
          (values (if (= start 0) #f start)
                  (if (= end 0)   #f end))
          (let ((p (car parts)))
            (loop (min start (part-start-lba p))
                  (max end (part-end-lba p))
                  (cdr parts))))))

  (define (parts-array-blocks blk-size)
    (fxdiv 16384 blk-size))

  (define (make-gpt-from-parts disk-id start end blk-size parts)
    (let-values (((part-start part-end) (find-part-start/end parts)))
      (let* ((disk-id (or disk-id (random-uuid)))
             (parts-array-size (parts-array-blocks blk-size))
             (start-lba (or start part-start (+ parts-array-size 2)))
             (end-lba (or (if end
                              (fx- end (fx+ parts-array-size 1))
                              #f)
                          part-end
                          (+ parts-array-size 2)))
             (alt-lba (+ end-lba parts-array-size 1))
             (size #f))
        (make-gpt #t disk-id parts #x00010000
                     alt-lba start-lba end-lba))))

  (let loop ((disk-id #f) (start 34) (end #f) (blk-size 512) (parts '()) (lst lst))
    (if (null? lst)
        (make-gpt-from-parts disk-id start end blk-size parts)
        (let ((kv (car lst)))
          (if (and (list? kv) (not (null? kv)))
              (let ((k (car kv)))
                (if (>= (length kv) 2)
                    (case k
                      ((disk-id)
                       (let ((disk-id (cadr kv)))
                         (when (not (string? disk-id))
                           (error 'sexp->gpt "Invalid disk-id value" disk-id))
                         (loop (string->uuid disk-id) start end blk-size parts (cdr lst))))
                      ((start)
                       (let ((start (cadr kv)))
                         (when (not (number? start))
                           (error 'sexp->gpt "Invalid start lba value" start))
                         (loop disk-id start end blk-size parts (cdr lst))))
                      ((end)
                       (let ((end (cadr kv)))
                         (when (not (number? end))
                           (error 'sexp->gpt "Invalid end lba value" end))
                         (loop disk-id start end blk-size parts (cdr lst))))
                      ((block-size)
                       (let ((blk-size (cadr kv)))
                         (when (not (number? blk-size))
                           (error 'sexp->gpt "Invalid block-size value" blk-size))
                         (loop disk-id start end blk-size parts (cdr lst))))
                      ((part)
                       (loop disk-id start end blk-size
                             (append parts
                                     (list (sexp->part (+ (length parts) 1) (cdr kv))))
                             (cdr lst)))
                      (else
                       (loop disk-id start end blk-size parts (cdr lst))))
                    (loop disk-id start end blk-size parts (cdr lst))))
              (loop disk-id start end blk-size parts (cdr lst)))))))

(define (create-gpt! gpt read-block write-block!)
  (define parts-array (make-bytevector (fx* 128 (length (parttable-partitions gpt)))))
  (define blk-size (bytevector-length (read-block 0)))

  (define (prepare-gpt-hdr hdr my-lba alternate-lba parts-lba)
    (pack! "<Q" hdr #x18 my-lba)
    (pack! "<Q" hdr #x20 alternate-lba)
    (pack! "<Q" hdr #x48 parts-lba)
    (pack! "<L" hdr #x10 (gpt-header-crc hdr)))

  (for-each
    (lambda (p)
      (let* ((offs (fx* (fx- (part-number p) 1) 128))
             (name0 (string-append (gptpart-name p) (make-string 1 #\nul)))
             (name-bv (string->utf16 name0 (endianness little)))
             (name (make-bytevector 72)))
        (bytevector-copy! name-bv 0 name 0 (min 72 (bytevector-length name-bv)))
        (bytevector-copy! (guid-swap (part-type p)) 0
                          parts-array (+ offs 0)
                          16)
        (bytevector-copy! (guid-swap (gptpart-guid p)) 0
                          parts-array (+ offs 16)
                          16)
        (pack! "<QQQ" parts-array (+ offs 32)
               (part-start-lba p)
               (part-end-lba p)
               0)
        (bytevector-copy! name 0 parts-array (+ offs 56) 72)))
    (parttable-partitions gpt))

  (let ((rev (gpt-revision gpt))
        (header-size #x5c)
        (alternate-lba (gpt-alternate-lba gpt))
        (first-usable-lba (gpt-first-usable-lba gpt))
        (last-usable-lba (gpt-last-usable-lba gpt))
        (disk-id (guid-swap (parttable-disk-id gpt)))
        (parts-num (length (parttable-partitions gpt)))
        (entry-size 128)
        (parts-crc (crc-32 parts-array)))
    (let ((hdr (make-bytevector blk-size)))
      (pack! "<QLL4x4x 8x3Q 16x 8xLLL" hdr 0
             GPT-SIGNATURE
             rev
             header-size
             ;;crc0
             ;;reserved
             ;;my-lba
             alternate-lba
             first-usable-lba
             last-usable-lba
             ;;disk-id
             ;;parts-lba
             parts-num
             entry-size
             parts-crc)
      (bytevector-copy! disk-id 0 hdr #x38 16)
      ;; prepare & write primary hdr
      (prepare-gpt-hdr hdr 1 alternate-lba 2)
      (write-block! hdr 1)
      (write-block! parts-array 2)
      ;; prepare & write backup hdr
      (prepare-gpt-hdr hdr alternate-lba 1 (+ last-usable-lba 1))
      (write-block! hdr alternate-lba)
      (write-block! parts-array (+ last-usable-lba 1)))))

(define (create-gpt+mbr! gpt read-block write-block!)
  (let ((mbr (make-mbr #t 0
               (list (make-mbrpart 1
                       1
                       (gpt-alternate-lba gpt)
                       (gpt-alternate-lba gpt)
                       #xEE
                       1
                       0
                       (lba->chs 1)
                       (lba->chs (gpt-alternate-lba gpt)))))))
    (create-mbr! mbr read-block write-block!)
    (create-gpt! gpt read-block write-block!)))
)
